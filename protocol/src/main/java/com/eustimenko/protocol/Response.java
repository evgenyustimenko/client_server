package com.eustimenko.protocol;

import java.io.Serializable;

public class Response implements Serializable {

    public final Integer id;
    public final Object attach;
    public final ResponseErrorType type;

    public Response(Integer id) throws Exception {
        this(id, Void.TYPE.newInstance());
    }

    public Response(Integer id, Object attach) {
        this(id, attach, ResponseErrorType.SUCCESS);
    }

    public Response(Integer id, ResponseErrorType type) throws Exception {
        this(id, Void.TYPE.newInstance(), type);
    }

    public Response(Integer id, Object attach, ResponseErrorType type) {
        this.id = id;
        this.attach = attach;
        this.type = type;
    }

    public String toString() {
        return id.toString();
    }
}
