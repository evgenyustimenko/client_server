package com.eustimenko.protocol;

import java.io.Serializable;

public class Request implements Serializable {

    public final Integer id;
    public final String serviceName;
    public final String methodName;
    public final Object[] parameters;

    private Request(Builder builder) {
        id = builder.id;
        serviceName = builder.serviceName;
        methodName = builder.methodName;
        parameters = builder.parameters;
    }

    public static class Builder {
        private final Integer id;

        private String serviceName = "default";
        private String methodName = "default";
        private Object[] parameters = new Object[]{};

        public Builder(Integer id) {
            this.id = id;
        }

        public Builder serviceName(String value) {
            serviceName = value;
            return this;
        }

        public Builder methodName(String value) {
            methodName = value;
            return this;
        }

        public Builder parameters(Object[] value) {
            parameters = value;
            return this;
        }

        public Request build() {
            return new Request(this);
        }
    }

    public String toString() {
        return serviceName + ":" + id.toString();
    }
}