package com.eustimenko.protocol;

public enum ResponseErrorType {

    SUCCESS,
    SERVICE_NOT_EXISTS,
    METHOD_NOT_EXISTS,
    INVALID_PARAMETERS_TYPE,
    INVALID_PARAMETERS_COUNT
}
