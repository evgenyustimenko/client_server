package com.eustimenko.server.exception;

import com.eustimenko.protocol.ResponseErrorType;

public class IncorrectResponseException extends RuntimeException {

    public final ResponseErrorType type;

    public IncorrectResponseException(ResponseErrorType type) {
        this.type = type;
    }
}
