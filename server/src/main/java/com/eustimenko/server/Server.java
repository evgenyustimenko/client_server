package com.eustimenko.server;

import com.eustimenko.protocol.*;
import com.eustimenko.server.exception.IncorrectResponseException;
import org.slf4j.*;

import java.io.*;
import java.lang.reflect.*;
import java.net.*;
import java.util.*;

public class Server implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(Server.class);
    private static final int DEFAULT_SERVER_PORT = 9999;

    private final ServerSocket listener;
    private final Map<String, Object> services;
    private volatile boolean keepProcessing = true;

    public Server(Map<String, Object> services) throws IOException {
        this(DEFAULT_SERVER_PORT, services);
    }

    public Server(int port, Map<String, Object> services) throws IOException {
        listener = new ServerSocket(port);
        this.services = services;
    }

    public void run() {
        logger.info("Server is started of port {}", listener.getLocalPort());
        while (keepProcessing) {
            try {
                final Socket socket = listener.accept();
                handleRequest(socket);
            } catch (Exception e) {
                handle(e);
            }
        }
    }

    public void stopProcessing() {
        keepProcessing = false;
        closeIgnoringException(listener);
    }

    private void handleRequest(Socket socket) throws Exception {
        if (socket == null) return;

        final Runnable clientHandler = () -> handleClient(socket);
        final Thread clientConnection = new Thread(clientHandler);
        clientConnection.start();
    }

    private void handleClient(Socket socket) {
        ObjectInputStream is = null;
        ObjectOutputStream os = null;
        try {
            is = new ObjectInputStream(socket.getInputStream());
            final Request command = (Request) is.readObject();
            logger.info("Accept command: {}", command.toString());

            final Response response = processRequest(command);

            os = new ObjectOutputStream(socket.getOutputStream());
            os.writeObject(response);
            os.flush();

            logger.info("Sent: {}", response.toString());
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            closeIgnoringException(socket, is, os);
        }
    }

    private Response processRequest(Request request) {
        final Object serviceObject = services.get(request.serviceName);

        if (serviceObject == null) {
            return new Response(request.id, request.serviceName, ResponseErrorType.SERVICE_NOT_EXISTS);
        } else if (request.methodName != null && !request.methodName.isEmpty()) {
            final Method[] methods = serviceObject.getClass().getDeclaredMethods();
            return getResponseViaMethodCall(methods, request, serviceObject);
        } else {
            return new Response(request.id, serviceObject);
        }
    }

    private Response getResponseViaMethodCall(Method[] methods, Request request, Object obj) {
        final List<Method> launchedMethods = new ArrayList<>(methods.length);
        for (final Method m : methods) {
            if (m.getName().equals(request.methodName)) launchedMethods.add(m);

        }
        if (launchedMethods.isEmpty()) {
            return new Response(request.id, request.serviceName, ResponseErrorType.METHOD_NOT_EXISTS);
        }

        Method called = null;
        for (final Method m : launchedMethods) {
            try {
                called = getCalledMethod(m, request);
            } catch (IncorrectResponseException e) {
                return new Response(request.id, request.serviceName, e.type);
            }
        }

        try {
            return new Response(request.id, called.invoke(obj, request.parameters));
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    private Method getCalledMethod(Method m, Request request) {
        Method called = null;

        final boolean isParametersCountEqual = m.getParameterCount() == request.parameters.length;
        if (isParametersCountEqual) called = m;
        if (called == null) throw new IncorrectResponseException(ResponseErrorType.INVALID_PARAMETERS_COUNT);

        called = null;
        final Class<?>[] calledMethodParameters = new Class[request.parameters.length];
        for (int i = 0; i < request.parameters.length; i++) {
            calledMethodParameters[i] = request.parameters[i].getClass();
        }

        if (isParametersTypesEqual(calledMethodParameters, m.getParameterTypes())) called = m;
        if (called == null) throw new IncorrectResponseException(ResponseErrorType.INVALID_PARAMETERS_TYPE);

        return called;
    }

    private boolean isParametersTypesEqual(Class<?>[] actual, Class<?>[] expected) {
        for (int i = 0; i < actual.length; i++) {
            if (!actual[i].equals(expected[i])) {
                return false;
            }
        }
        return true;
    }

    private void closeIgnoringException(Socket socket, ObjectInputStream is, ObjectOutputStream os) {
        if (is != null) {
            try {
                is.close();
            } catch (IOException ignored) {
            }
        }
        if (os != null) {
            try {
                os.close();
            } catch (IOException ignored) {
            }
        }
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException ignored) {
            }
        }
    }

    private void closeIgnoringException(ServerSocket serverSocket) {
        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException ignored) {
            }
        }
    }

    private void handle(Exception e) {
        if (!(e instanceof SocketException)) {
            logger.error(e.getMessage());
        }
    }
}
