package com.eustimenko.server.service;

import org.slf4j.*;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class ServiceHelper {

    private static final Logger logger = LoggerFactory.getLogger(ServiceHelper.class);
    private static final String FILENAME = "services.properties";
    private static final String searchPackages = "com.eustimenko.server.service";

    private Properties services;

    public Map<String, Object> getServices() throws IOException {
        openProperties();
        return createServices();
    }

    private void openProperties() throws IOException {
        services = new Properties();
        final InputStream input = ServiceHelper.class.getClassLoader().getResourceAsStream(FILENAME);
        if (input == null) {
            logger.error("File {} is not exists", FILENAME);
        } else {
            services.load(input);
        }
    }

    private Map<String, Object> createServices() {
        final Map<String, Object> objectMap = new ConcurrentHashMap<>(services.size());
        services.stringPropertyNames().parallelStream().forEach(s -> {
            final String className = services.getProperty(s);
            try {
                objectMap.put(s, findClassByName(className).newInstance());
                logger.info("Create service with name {} and with class {}", s, className);
            } catch (InstantiationException | IllegalAccessException e) {
                logger.error("The class {} can't be instatiated. See InstantiationException or IllegalAccessException java docs", className);
            } catch (ClassNotFoundException e) {
                logger.warn(e.getMessage());
            }
        });

        return objectMap;
    }

    private Class<?> findClassByName(String name) throws ClassNotFoundException {
        try {
            return Class.forName(searchPackages + "." + name);
        } catch (ClassNotFoundException e) {
            throw new ClassNotFoundException("Class " + name + " is not found at " + searchPackages, e);
        }
    }
}
