package com.eustimenko.server.service;

import java.io.Serializable;
import java.util.Date;

public class Service1 implements Serializable {

    public void sleep(Long millis) throws InterruptedException {
        Thread.sleep(millis.longValue());
    }

    public Date getCurrentDate() {
        return new Date();
    }

}
