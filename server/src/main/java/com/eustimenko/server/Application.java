package com.eustimenko.server;

import com.eustimenko.server.service.ServiceHelper;
import org.slf4j.*;

import java.io.*;
import java.util.*;

public class Application {

    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) throws IOException {

        if (args.length > 1) {
            validateArguments();
            System.exit(0);
        }

        final ServiceHelper helper = new ServiceHelper();
        final Map<String, Object> services = helper.getServices();

        Server server = null;
        try {
            server = args.length == 1 ? new Server(getPortFromString(args[0]), services) : new Server(services);
            server.run();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            if (server != null) {
                server.stopProcessing();
            }
            System.exit(0);
        }

    }

    private static void validateArguments() {
        logger.error("Arguments count should be equals or less then 1");
        logger.info("Command format: java -jar server.jar [port number]");
    }

    private static int getPortFromString(String port) {
        try {
            return Integer.valueOf(port);
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Argument should have number format");
        }
    }

}