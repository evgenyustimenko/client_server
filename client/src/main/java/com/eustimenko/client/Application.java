package com.eustimenko.client;

import com.eustimenko.client.exception.IncorrectResponseException;
import org.slf4j.*;

import java.io.*;
import java.net.*;

public class Application {

    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {

        if (args.length > 2 || args.length == 1) {
            validateArguments();
            System.exit(0);
        }

        try {
            final Client client = args.length == 2 ? new Client(args[0], getPortFromString(args[1])) : new Client();
            for (int i = 0; i < 10; i++) {
                new Thread(new Caller(client)).start();
            }
        } catch (IOException e) {
            logger.error("The process occurred error {}", e.getMessage());
        }

    }

    private static void validateArguments() {
        logger.error("Invalidate command format");
        logger.info("Command format: java -jar client.jar [hostname] [port number]");
    }

    private static int getPortFromString(String port) {
        try {
            return Integer.valueOf(port);
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Argument should have number format");
        }
    }
}