package com.eustimenko.client.exception;

import com.eustimenko.protocol.*;

public class IncorrectResponseException extends RuntimeException {

    private final Response response;

    public IncorrectResponseException(Response response) {
        this.response = response;
    }

    public String getMessage() {
        switch (response.type) {
            case SERVICE_NOT_EXISTS:
                return "Service does not exists. Command id: " + response.id + ". Service name: " + response.attach.toString();
            case INVALID_PARAMETERS_COUNT:
                return "Invalid parameters count. Command id: " + response.id + ". Service name: " + response.attach.toString();
            case INVALID_PARAMETERS_TYPE:
                return "Invalid parameters type. Command id: " + response.id + ". Service name: " + response.attach.toString();
            case METHOD_NOT_EXISTS:
                return "Method is not exists. Command id: " + response.id + ". Service name: " + response.attach.toString();
            default:
                return super.getMessage();
        }
    }
}
