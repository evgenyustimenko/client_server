package com.eustimenko.client;

import com.eustimenko.client.exception.IncorrectResponseException;
import com.eustimenko.protocol.*;
import org.slf4j.*;

import java.io.*;
import java.net.*;
import java.util.concurrent.Callable;

public class Client {

    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    private static final String DEFAULT_HOST = "localhost";
    private static final int DEFAULT_PORT = 9999;

    private final String host;
    private final int port;
    private volatile int id;

    public Client() throws IOException {
        this(DEFAULT_HOST, DEFAULT_PORT);
    }

    public Client(String host, int port) throws IOException {
        this.host = host;
        this.port = port;
        logger.info("Client is started on {}:{}", host, port);
    }

    public Object remoteCall(String serviceName, String methodName, Object[] params) throws Exception {
        final Request command = new Request.Builder(++id)
                .serviceName(serviceName)
                .methodName(methodName)
                .parameters(params)
                .build();

        final Socket socket = new Socket(host, port);

        final ObjectOutputStream os = new ObjectOutputStream(socket.getOutputStream());
        os.writeObject(command);
        os.flush();

        final ObjectInputStream is = new ObjectInputStream(socket.getInputStream());

        Object result;
        final Response response = (Response) is.readObject();
        switch (response.type) {
            case SUCCESS:
                logger.info("Command: {}", response.id);
                result = response.attach;
                break;
            default:
                throw new IncorrectResponseException(response);
        }

        os.close();
        is.close();

        return result;

    }
}
