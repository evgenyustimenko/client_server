package com.eustimenko.client;

import org.slf4j.*;

import java.io.IOException;
import java.net.SocketException;

public class Caller implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(Caller.class);
    private final Client client;

    public Caller(Client client) {
        this.client = client;
    }

    public void run() {
        while (true) {
            try {
                client.remoteCall("service1", "sleep", new Object[]{new Long(1000)});
                logger.info("Current Date is:" + client.remoteCall("service1", "getCurrentDate", new Object[]{}));
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            } catch (Exception e) {
                if (!(e instanceof SocketException)) {
                    logger.error(e.getMessage());
                }
            }
        }
    }
}
