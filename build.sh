#Publish protocol dependency to maven local directory
cd protocol
./gradlew publishToMavenLocal

#Build server
cd ../server
./gradlew clean fatJar

#Build client
cd ../client
./gradlew clean fatJar
