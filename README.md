# Client-Server with fake TCP-protocol

## Dependencies
1. Java 1.8

## Modules
`server` - Multithreading TCP-server, receiving commands from client and sending

`client` - TCP-client, sending commands to server by separate threads

`protocol` - contains fake protocol commands

## Build
See [build.sh](../build.sh)

## Launch
To launch server and client you should call

1. `java -jar server/build/libs/server-0.0.1.jar`
1. `java -jar client/build/libs/client-0.0.1.jar`

from root directory

## Services
To add new service you should do the following:

1. Create new class at `com.eustimenko.server.service` package to `server`-module
1. Add `<key,value>`-pair into `services.properties` file

### Example:
1. New class with name `Service1` is created at `com.eustimenko.server.service`
1. The `services.properties` file should contains the following `newService=Service1`